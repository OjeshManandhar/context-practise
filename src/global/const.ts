export const A4Width = '210mm';
export const A4Padding = '0.5in 1in';

export const HeaderHeight = '3.6rem';

export const Transition = 'transition: 0.5s ease all;';
