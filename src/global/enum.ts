export enum LOCALE {
  NEPALI = 'नेपाली',
  ENGLISH = 'English'
}

export enum THEME {
  DARK = 'Dark',
  LIGHT = 'Light'
}
