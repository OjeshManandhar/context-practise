// enum
import { THEME, LOCALE } from './enum';

// type Obj = {
//   [index: string]: {
//     [index: string]: {
//       [index: string]: string;
//     };
//   };
// };

const Locale = {
  [LOCALE.ENGLISH]: {
    theme: {
      [THEME.DARK]: THEME.DARK,
      [THEME.LIGHT]: THEME.LIGHT
    },
    description: {
      name: 'Ojesh Manandhar',
      location: 'Buddhanagar, Kathmandu',
      contact: '+977-9863198269',
      email: 'ozes.manandhar@gmail.com',
      gitHub: 'github.com/OjeshManandhar',
      linkedIn: 'linkedin.com/in/ozes-manandhar/',
      about: [
        'I’m a tech enthusiast and striving to become a better person than I am today by honing my developing, designing, managing, leading and soft skills.',
        'I have been working in frontend using ReactJS and React Native for a while and now I have been self-learning NodeJS to gain some backend development skill to become a full-stack JS developer.'
      ]
    },
    experience: {
      heading: 'Experience',
      items: [
        {
          time: 'Present',
          title: 'Freelancing',
          subTitle: 'KaiOS Apps',
          list: [
            'Made small and simple website using HTML, CSS, JS and ReactJS, which would run as apps in devices running KaiOS',
            'Worked on multiple projects alone and some with other people',
            'Worked as a lead developer in one project'
          ]
        },
        {
          time: 'December 2019 - March 2020',
          title: 'Jr. Software Engineer',
          subTitle: 'Core Software Integrated',
          list: [
            'Developed responsive landing page using ReactJS',
            'Integrated multiple payment system in website',
            'Developed mobile app using React Native',
            'Worked with maps using Mapbox in React Native',
            'Worked with TypeScript and StyledComponents and in a specific styled guide'
          ]
        },
        {
          time: 'August 2017 - April 2021',
          title: 'Member',
          subTitle: 'KEC IT Club',
          list: [
            'As Secretary of the club I was mostly involved in bridging the gap between college administration and other clubs with this club',
            'Managed bootcamps and other events',
            'Contact IT companies for workshop or sponsorship for events'
          ]
        }
      ]
    },
    education: {
      heading: 'Education',
      items: [
        {
          time: 'April 2021',
          title: 'Bachelors',
          subTitle: 'Kathmandu Engineering College (Tribhuvan University)'
        },
        {
          time: 'July 2016',
          title: '+2 (HSEB)',
          subTitle: 'Ararsha Vidya Mandir Higher Secondary School'
        },
        {
          time: 'April 2014',
          title: 'SLC',
          subTitle: 'Ararsha Vidya Mandir Higher Secondary School'
        }
      ]
    },
    skills: {
      heading: 'Skills',
      have: {
        heading: 'I have',
        items: [
          'C and C++',
          'WordPress',
          'HTML and CSS',
          'JavaScript and TypeScript',
          'React and React-Native',
          'MS Word, MS Excel and MS Visio',
          'Firebase',
          'Git and GitHub',
          'KaiOS',
          'Mapbox'
        ]
      },
      learning: {
        heading: 'Self-learning',
        items: ['Express.JS', 'SQL and NoSQL Database', 'REST APIs', 'GraphQL']
      }
    },
    project: {
      heading: 'Project',
      items: [
        {
          heading: 'Kharcha – Web',
          description:
            'Kharcha – Web is project I’m currently working on to learn Backend through JS by making firstly a simple web-site almost similar to Kharcha made using C, then making a better version, with more features using ReactJS and GraphQL and later on to a mobile app using React Native. This is also an opportunity to learn cloud services like AWS, Heroku, etc.'
        },
        {
          heading: 'RATS – Real-time Ambulance Traffic Support',
          description: [
            'This is my project for the final year of my Bachelors. The main purpose of this app is to gap the bridge between Traffic operators and Ambulance drivers for quickly reaching to hospital/patient on time. I worked on the android apps of this project, called LifeLine, which handles the map interface, adding of route taken by driver and obstruction in different place placed by traffic.',
            'Both apps are private repository in my GitHub. The repository LifeLine-App-Test is the test app I made before starting the actual app for the project.'
          ]
        },
        {
          heading: 'MediAid',
          description: [
            'This project is my minor project for the 3rd year of my Bachelors study. MediAid is a mobile app made using React Native whose major objective is to provide users easy access to health facilities and keep them updated on their medical schedule and available medical events.',
            'MediAid is currently a private repository in my GitHub. The repository MediAid Test contains some of the test codes I did while coding.'
          ]
        },
        {
          heading: 'Experience Nepal',
          description:
            'A website to help improve the Tourism Industry of Nepal along with helping for the grand success of Visit Nepal 2020 project. Made this website while participating different competitions. There is still a lot of improvement to do in this website along with making an official mobile app.'
        },
        {
          heading: 'Club site (GitHub repo: WordPress-site)',
          description:
            'A website made for KEC IT Club for practicing WordPress.'
        },
        {
          heading: 'Kharcha',
          description:
            'A program made in C language to keep record of daily transaction by creating a user account and recording the savings and spending, in a tag-based way, in local file system.'
        }
      ],
      info: 'All details regarding the Projects are properly described in the respective GitHub repository.'
    },
    participations: {
      heading: 'Participations',
      items: [
        { event: 'AI Expo', date: 'August 2019' },
        { event: 'Everest Hack 2019', date: 'July 2019' },
        {
          event: 'Hackathon at Sagarmatha Techno-Fest 2019',
          date: 'June 2019'
        },
        { event: 'LOCUS Codejam 2019', date: 'January 2019' },
        { event: 'LOCUS HACK-A-WEEK', date: 'January 2019' },
        { event: 'Software Exhibition at Kathfest', date: 'January 2019' },
        { event: 'AlCode at Kathfest', date: 'January 2019' },
        { event: 'Coding Challenge at KU IT Meet', date: 'December 2018' },
        { event: 'Hackathon at KU IT Meet', date: 'December 2018' },
        {
          event: 'Paper Presentation at OKRP Conference 2018',
          date: 'December 2018'
        },
        { event: 'RoboDrift', date: 'July 2018' },
        { event: 'Coding Competition at Yatra 1.0', date: 'February 2018' },
        { event: 'LOCUS Codejam 2018', date: 'February 2018' },
        {
          event: 'Coding Tournament and Coding Competition at KU IT Meet',
          date: 'January 2018'
        },
        { event: 'Circuit Synthesis and Coding', date: 'June 2017' },
        { event: 'LOCUS Codejam 2016', date: 'June 2016' }
      ]
    },
    achievements: {
      heading: 'Achievements and Certifications',
      items: [
        {
          time: 'January 2019',
          title: 'AlCode',
          about: [
            'Winner',
            'A coding competition in Kathfest organised by Kathford International College of Engineering and Management'
          ],
          description:
            'Problem solving based competition where participants had to solve six problems each having different marks using C or C++.'
        },
        {
          time: 'December 2018',
          title: 'Paper Presentation',
          about:
            'Office of KEC Research and Publication (OKRP) is a platform to present research papers organised by Kathmandu Engineering College (KEC) annually',
          description:
            'I along with Samip Shrestha and Dharatee Shree Shrestha under the guidance of Er. Pratik Shrestha presented a research paper “Digitization of Stock Market for future analysis”.'
        },
        {
          time: 'July 2018',
          title: 'Code X',
          about: [
            'Winner of 2nd round',
            'A competition in Shrijana The Innovation organised by Advanced College of Engineering and Management'
          ],
          description:
            'Competed against 4 other participants where we had to solve a single problem under 30 minutes using any language we preferred.'
        },
        {
          time: 'July 2018',
          title: 'RoboDrift',
          about: ['2nd runner-up', 'Organized by KEC Robotics Club'],
          description: [
            'The goal was to build a bot/car which could work both manually (wireless, BT) andautomatically. In automatic part of the competition, the bot had to go through a tunnel only using the sensors (Ultra Sound) installed in it.',
            'Teamed with Sushmita Rai and Samip Shrestha.'
          ]
        },
        {
          time: 'June 2018',
          title: 'Yoga',
          about: 'Organized by KEC Yoga Club',
          description:
            'It is a certificate awarded to all students who had complete attendance in Yoga class in their first year.'
        },
        {
          time: 'June 2017,',
          title: 'Circuit Synthesis and Coding',
          about: [
            'Winner of Circuit Synthesis and Coding',
            'Organized by KEC Robotics Club'
          ],
          description: [
            'It was an intra college competition where participants had to connect the given circuit in the first part and a coding competition in the second part.',
            'Teamed with Samip Shrestha.'
          ]
        }
      ]
    },
    volunteering: {
      heading: 'Volunteering/Organising',
      items: [
        {
          time: 'July 2019',
          title: 'Organised different IT events at Drisit 2.0',
          description:
            'As the vice-secretory of KEC IT Club, I managed different events organised by the club which included Hackathon, Software Exhibition, Quick Code N Win and GameFest (CS:GO, FIFA 19 and PUBG). I was also engaged in wall painting in the event floor.'
        },
        {
          time: 'July 2019',
          title: 'Volunteering at OKRP 2018',
          description:
            'Volunteered at Office of KEC Research and Publication (OKRP) 2018 and managed the registration of different honoured guests, respected teachers and fellow participants.'
        }
      ]
    }
  },
  [LOCALE.NEPALI]: {
    theme: {
      [THEME.DARK]: 'अध्यारो',
      [THEME.LIGHT]: 'उज्यालो'
    },
    description: {
      name: 'ओजेश मानन्धर',
      location: 'बुद्धनगर, काठमाडौँ',
      contact: '+९७७-९८६३१९८२६९',
      email: 'ozes.manandhar@gmail.com',
      gitHub: 'github.com/OjeshManandhar',
      linkedIn: 'linkedin.com/in/ozes-manandhar/',
      about: [
        'म एक प्राविधिक उत्साही हुँ र मेरो विकास गर्ने, डिजाईन गर्ने, प्रबन्ध गर्ने, प्रमुख र नरम सीपहरूको सम्मान गर्दै आज भन्दा म राम्रो व्यक्ति बन्ने प्रयास गर्दैछु।',
        'मैले ReactJS र React-Native प्रयोग गरेर फ्रन्टएन्डमा काम गर्दैछु र अब म पूर्ण-स्ट्याक JS विकासकर्ता बन्नको लागि केही ब्याकइन्ड विकास सीप प्राप्त गर्न N आodeJS त्म-सीख्दै छु।'
      ]
    },
    experience: {
      heading: 'अनुभव',
      items: [
        {
          time: 'प्रस्तुत',
          title: 'फ्रीलान्सिंग',
          subTitle: 'KaiOS एप्स',
          list: [
            'HTML, CSS, JS र ReactJS प्रयोग गरेर साना र साधारण वेबसाइटहरू निर्माण गर्नुहोस्, जुन KaiOS चलाउने उपकरणहरूमा एप्लिकेसनको रूपमा चल्नेछ',
            'एक्लै बहु परियोजनाहरुमा काम गर्नुभयो र केहि अन्य मानिसहरुसंग',
            'एउटा परियोजनामा एक नेतृत्व विकासकर्ताको रूपमा काम गरे'
          ]
        },
        {
          time: 'डिसेम्बर २०१९ - मार्च २०२०',
          title: 'जूनियर सफ्टवेयर ईन्जिनियर',
          subTitle: 'Core Software Integrated',
          list: [
            'ReactJS प्रयोग गरेर उत्तरदायी ल्यान्डि page पृष्ठको विकास भयो',
            'वेबसाइटमा बहु एकीकृत एकाधिक भुक्तानी प्रणाली',
            'React Native प्रयोग गरेर विकसित मोबाइल अनुप्रयोग',
            'React Native मा Mapboxको प्रयोग गरेर नक्साका साथ काम गरीयो',
            'TypeScript र StyledCompoonents काम गरीएको र एक विशेष स्टाइलको गाईडमा'
          ]
        },
        {
          time: 'अगस्त २०१७ - अप्रिल २०२१',
          title: 'सदस्य',
          subTitle: 'KEC IT Club',
          list: [
            'क्लबको सेक्रेटरी भएको नातामा म कलेज प्रशासन र यस क्लबको साथमा अन्य क्लबहरूको बिचको दूरी हटाउन प्रायः संलग्न थिएँ',
            'Bootcamps र अन्य घटनाहरू व्यवस्थित गरियो',
            'कार्यशाला वा घटनाहरूको लागि प्रायोजनको लागि IT कम्पनीहरूलाई सम्पर्क गरे'
          ]
        }
      ]
    },
    education: {
      heading: 'शिक्षा',
      items: [
        {
          time: 'अप्रिल २०२१',
          title: 'स्नातक',
          subTitle: 'काठमाडौं ईन्जिनियरि कलेज (त्रिभुवन विश्वविद्यालय)'
        },
        {
          time: 'जुलाई २०१६',
          title: '+२ (एह.एस.ई.बी.)',
          subTitle: 'आदर्श विद्या मन्दिर उच्च माध्यमिक विद्यालय'
        },
        {
          time: 'अप्रिल २०१४ ',
          title: 'एस.एल.सी.',
          subTitle: 'आदर्श विद्या मन्दिर उच्च माध्यमिक विद्यालय'
        }
      ]
    },
    skills: {
      heading: 'कौशल',
      have: {
        heading: 'मसँग भएको',
        items: [
          'C and C++',
          'WordPress',
          'HTML and CSS',
          'JavaScript and TypeScript',
          'React and React-Native',
          'MS Word, MS Excel and MS Visio',
          'Firebase',
          'Git and GitHub',
          'KaiOS',
          'Mapbox'
        ]
      },
      learning: {
        heading: 'आफै सिकिरहेको ',
        items: ['Express.JS', 'SQL and NoSQL Database', 'REST APIs', 'GraphQL']
      }
    },
    project: {
      heading: 'Project',
      items: [
        {
          heading: 'Kharcha – Web',
          description:
            'Kharcha - Web परियोजना मँ अहिले JS मार्फत ब्याकएन्ड सिक्न काम गर्दैछुँ, पहिले C  प्रयोग गरी बनाएको Kharcha संग मिल्दोजुल्दो साधारण वेब-साइट बनाई, त्यसपछि राम्रो संस्करण बनाएर, ReactJS र GraphQL प्रयोग गरेर अधिक सुविधाहरूको साथ पछि मोबाइल अनुप्रयोग React-Native प्रयोग गर्दै। यो क्लाउड सेवाहरू जस्तै AWS, Heroku, आदि सिक्ने अवसर पनि हो।'
        },
        {
          heading: 'RATS – Real-time Ambulance Traffic Support',
          description: [
            'यो मेरो स्नातक को अन्तिम वर्ष को लागी मेरो परियोजना हो। यस अनुप्रयोगको मुख्य उद्देश्य भनेको ट्राफिक अपरेटरहरू र एम्बुलेन्स ड्राइभरहरूको बिचको दूरी घटाउनु हो जसले गर्दा समयमै अस्पताल / बिरामीकोमा पुग्न सक्छ। मैले यस प्रोजेक्टको android अनुप्रयोगहरूमा काम गरें, जस्लाई LifeLine नाम देको छौं, जसले नक्शा इन्टरफेस, ड्राइभरद्वारा लिइएको मार्ग र ट्राफिकले राखेको बिभिन्न ठाउँमा रोकावट ह्यान्डल गर्दछ।',
            'दुबै अनुप्रयोगहरू मेरो GitHubमा निजी भण्डारमा राखिएको छ। भण्डारन LifeLine-App-Test परियोजनाको लागि वास्तविक अनुप्रयोग सुरू गर्नु अघि मैले बनाएको परीक्षण अनुप्रयोग हो।'
          ]
        },
        {
          heading: 'MediAid',
          description: [
            'यो परियोजना मेरो स्नातक अध्ययनको तेस्रो वर्षको लागि मेरो सानो परियोजना हो। MediAid एक मोबाइल अनुप्रयोग हो जुन React-Native प्रयोग गरेर बनाइएको हो जसको प्रमुख उद्देश्य भनेको प्रयोगकर्ताहरूलाई स्वास्थ्य सुविधाको सहज पहुँच प्रदान गर्नु हो र उनीहरूको मेडिकल तालिका र उपलब्ध चिकित्सा घटनाहरूमा अद्यावधिक राख्नु हो।',
            'मेडिआइड हाल मेरो GitHubमा निजी भण्डारमा राखिएको छ। भण्डारन MediAid-Test कोडिंग गर्ने क्रममा मैले टेस्ट गर्ने केही कोडहरू समावेश गर्दछ।'
          ]
        },
        {
          heading: 'Experience Nepal',
          description:
            'नेपाल पर्यटन उद्योगलाई मद्दत पुर्‍याउ र विसित नेपाल २०२० को ठूलो सफलतालाई मद्दत पुर्युना बनाइएको वेब-सिटे हो। यो विभिन्न प्रतियोगितामा भाग लिदा बनाइएको वेब-साइट हो। त्यहाँ आधिकारिक मोबाइल अनुप्रयोग बनाउनुका साथै यस वेबसाइटमा अझ धेरै सुधार गर्नुपर्ने छ।'
        },
        {
          heading: 'Club site (GitHub भण्डारको नाम: WordPress-site)',
          description:
            'WordPress अभ्यासको लागि KEC IT Clubको लागि बनाईएको वेब-साइट हो।'
        },
        {
          heading: 'Kharcha',
          description:
            'स्थानीय फाइल प्रणालीमा ट्याग-आधारित तरीकामा प्रयोगकर्ता खाता सिर्जना गरेर र बचत र रिकर्डिंग गरेर दैनिक लेनदेनको रेकर्ड राख्न सी भाषामा बनाइएको कार्यक्रम।'
        }
      ],
      info: 'प्रोजेक्ट सम्बन्धी सबै विवरणहरू सम्बन्धित GitHub भण्डारमा ठीकसँग वर्णन गरिएको छ।'
    },
    participations: {
      heading: 'Participations',
      items: [
        { event: 'AI Expo', date: 'अगस्त २०१९' },
        { event: 'Everest Hack 2019', date: 'जुलाई २०१९' },
        {
          event: 'Hackathon at Sagarmatha Techno-Fest 2019',
          date: 'जुन २०१९'
        },
        { event: 'LOCUS Codejam 2019', date: 'जनवरी २०१९' },
        { event: 'LOCUS HACK-A-WEEK', date: 'जनवरी २०१९' },
        { event: 'Software Exhibition at Kathfest', date: 'जनवरी २०१९' },
        { event: 'AlCode at Kathfest', date: 'जनवरी २०१९' },
        { event: 'Coding Challenge at KU IT Meet', date: 'डिसेम्बर २०१८' },
        { event: 'Hackathon at KU IT Meet', date: 'डिसेम्बर २०१८' },
        {
          event: 'Paper Presentation at OKRP Conference २०१८',
          date: 'डिसेम्बर २०१८'
        },
        { event: 'RoboDrift', date: 'जुलाई २०१८' },
        { event: 'Coding Competition at Yatra 1.0', date: 'फेब्रुअरी २०१८' },
        { event: 'LOCUS Codejam 2018', date: 'फेब्रुअरी २०१८' },
        {
          event: 'Coding Tournament and Coding Competition at KU IT Meet',
          date: 'जनवरी २०१८'
        },
        { event: 'Circuit Synthesis and Coding', date: 'जुन २०१७' },
        { event: 'LOCUS Codejam 2016', date: 'जुन २०१६' }
      ]
    },
    achievements: {
      heading: 'उपलब्धिहरु र प्रमाणपत्रहरु',
      items: [
        {
          time: 'जनवरी २०१९',
          title: 'AlCode',
          about: [
            'विजेता',
            'Kathfestमा एक कोडिंग प्रतियोगिता क्याथफोर्ड ईन्टरनेशनल कलेज अफ इन्जिनियरि र व्यवस्थापन द्वारा आयोजित'
          ],
          description:
            'समस्या समाधान आधारित प्रतिस्पर्धा जहाँ प्रतिभागीहरूले छवटा समस्याहरू समाधान गर्नुपर्दथियो C वा C ++ को प्रयोग गरेर, जस्मा प्रत्येको बिभिन्न अंक थियो।'
        },
        {
          time: 'डिसेम्बर २०१८',
          title: 'पत्र प्रस्तुतिकरण',
          about:
            'Office of KEC Research and Publication (OKRP), काठमाडौं ईन्जिनियरि कलेज (के.ई.सी.) द्वारा वार्षिक रुपमा आयोजित अनुसन्धान पत्र प्रस्तुत गर्ने मंच',
          description:
            'मैले समिप श्रेष्ठ र धरती श्री श्रेष्ठसँग ईर. प्रतीक श्रेष्ठको निर्देशनमा एक शोध पत्र "Digitization of Stock Market for future analysis" प्रस्तुत गरेको।'
        },
        {
          time: 'जुलाई २०१८',
          title: 'Code X',
          about: [
            'दोस्रो चरणको विजेता',
            'Advanced College of Engineering and Management द्वारा आयोजित Shrijana The Innovation एक प्रतियोगिता'
          ],
          description:
            'अन्य ४ सहभागीहरूका विरुद्ध प्रतिस्पर्धा गरियो जहाँ हामीले एकल समस्या ३० मिनेट भित्रमा हामीले मन परेको कुनै प्रोग्रम्मिंग भाषा प्रयोग गरेर समाधान गर्नुपर्ने।'
        },
        {
          time: 'जुलाई २०१८',
          title: 'RoboDrift',
          about: ['दोस्रो रनर-अप', 'KEC Robotics Club द्वारा आयोजित'],
          description: [
            'लक्ष्य बट निर्माण गर्ने थियो जुन दुबै मैन्युअल (वायरलेस, बी.टी.) र स्वचालित रूपमा काम गर्न सक्नुपर्द थियो। प्रतिस्पर्धाको स्वचालित भागमा, बटले केवल यसमा स्थापित सेन्सरहरू (अल्ट्रा साउन्ड) को प्रयोग गरेर सुरुङ्ग पार गर्नु पर्ने थियो।',
            'सुष्मिता राई र समिप श्रेष्ठसँग मिलेर गरेको।'
          ]
        },
        {
          time: 'जुन २०१८',
          title: 'योग',
          about: 'KEC Yoga Club द्वारा आयोजित',
          description:
            'यो एक प्रमाणपत्र हो जुन सबै विद्यार्थीहरूलाई जो योग कक्षामा पूर्ण उपस्थिति वयेकोमा पाएको थियो।'
        },
        {
          time: 'जुन २०१७,',
          title: 'Circuit Synthesis and Coding',
          about: ['विजेता', 'KEC Robotics Club द्वारा आयोजित'],
          description: [
            'यो एक इंट्रा कलेज प्रतिस्पर्धा थियो जहाँ सहभागीहरूले पहिलो भागमा दिइएको सर्किट जडान गर्नुपर्ने र दोस्रो भाग कोडिंग प्रतिस्पर्धा थियो।',
            'समिप श्रेष्ठसँग मिलेर गरेको।'
          ]
        }
      ]
    },
    volunteering: {
      heading: 'स्वैच्छिक/आयोजन',
      items: [
        {
          time: 'जुलाई २०१९',
          title: 'Drisit 2.0 मा बिभिन्न IT घटनाहरू आयोजना गरियो',
          description:
            'KET IT Club को उपसचिवको रूपमा, मैले क्लबले आयोजना गरेको विभिन्न कार्यक्रमहरू Hackathon, Software Exhibition, Quick Code N Win र  GameFest (CS:GO, FIFA 19 र PUBG) व्यवस्थित गरेको थिए। म घटना वयेको तल्लाको भित्ता चित्रमा पनि व्यस्त थिए।'
        },
        {
          time: 'जुलाई २०१९',
          title: 'OKRP 2018 मा स्वैच्छिक गर्दा',
          description:
            'Office of KEC Research and Publication (OKRP) 2018 मा स्वयम्सेवक भई विभिन्न सम्मानित अतिथिहरू, सम्मानित शिक्षकहरू र सँगी सहभागीहरूको पंजीकरण प्रबन्ध गरेकोथिए।'
        }
      ]
    }
  }
};

export default Locale;
