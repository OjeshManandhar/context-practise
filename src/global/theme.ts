// enum
import { THEME } from './enum';

const Colors = {
  white: '#fbfbfb',
  black: '#303030',

  grey: '#9a8c98',

  green: '#1d824c',

  silver: '#c0c0c0',
  dimGrey: '#636363',

  blue: '#2c5c85',
  crayola: '#d3a37a',

  darkGrey: '#595959',

  sonicSilver: '#777777',
  silverChalice: '#acacac'
};

export const HeaderTheme: {
  [index: string]: {
    [index: string]: string;
  };
} = {
  [THEME.LIGHT]: {
    bg: Colors.white,
    border: Colors.black,
    activeBox: Colors.black,
    color: Colors.black,
    active: Colors.white,
    hover: Colors.grey
  },
  [THEME.DARK]: {
    bg: Colors.black,
    border: Colors.white,
    activeBox: Colors.white,
    color: Colors.white,
    active: Colors.black,
    hover: Colors.grey
  }
};

export const ContentTheme: {
  [index: string]: {
    [index: string]: string;
  };
} = {
  [THEME.LIGHT]: {
    bg: Colors.white,
    border: Colors.black,
    color: Colors.black,
    link: Colors.blue,
    text: Colors.darkGrey,
    title: Colors.green,
    listBorder: Colors.dimGrey,
    listItemBorder: Colors.sonicSilver
  },
  [THEME.DARK]: {
    bg: Colors.black,
    border: Colors.white,
    color: Colors.white,
    link: Colors.crayola,
    text: Colors.white,
    title: Colors.white,
    listBorder: Colors.silver,
    listItemBorder: Colors.silverChalice
  }
};
