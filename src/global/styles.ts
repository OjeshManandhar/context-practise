// packages
import styled from 'styled-components';

// global
import { THEME } from 'global/enum';
import { Transition } from 'global/const';
import { ContentTheme } from 'global/theme';

export const Container = styled.section<{ theme: THEME }>`
  margin-top: 2rem;

  font-size: 1.1rem;
  color: ${props => ContentTheme[props.theme].text};
`;

export const Heading = styled.h2<{ theme: THEME }>`
  font-size: 1.4rem;
  font-weight: bold;
  font-family: Georgia;
  text-transform: uppercase;
  color: ${props => ContentTheme[props.theme].title};

  ${Transition}
`;

export const List = styled.ul<{ theme: THEME }>`
  list-style-type: none;

  padding-left: 2rem;
  margin: 1rem 0.3rem;

  border-left: 0.25rem dotted ${props => ContentTheme[props.theme].listBorder};

  transition: 0.5s ease border;
`;

export const ListItem = styled.li<{ theme: THEME }>`
  margin-bottom: 0.5rem;

  border-bottom: 0.1rem solid
    ${props => ContentTheme[props.theme].listItemBorder};

  ${Transition}

  &:last-child {
    border: none;
  }
`;

export const Time = styled.div`
  font-weight: bold;
  text-transform: uppercase;
`;

export const Title = styled.div<{ uppercase: boolean; theme: THEME }>`
  font-size: 1.3rem;
  font-weight: bold;
  color: ${props => ContentTheme[props.theme].title};
  ${props => props.uppercase && 'text-transform: uppercase;'}

  margin-bottom: 0.1rem;

  ${Transition}
`;

export const SutTitle = styled.span<{ theme: THEME }>`
  font-weight: normal;
  text-transform: none;
  font-variant: small-caps;
  color: ${props => ContentTheme[props.theme].text};

  ${Transition}
`;

export const SubList = styled.ul`
  margin-left: 1.1rem;
  list-style: disc outside;
`;

export const SubListItem = styled.li`
  padding-left: 0.5rem;

  &::marker {
    font-size: 1.25rem;
  }
`;

export const Description = styled.p`
  margin-bottom: 0.25rem;

  white-space: pre-line;

  &:last-child {
    margin-bottom: 0;
  }
`;
