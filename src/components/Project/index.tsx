// packages
import React, { useContext } from 'react';

// context
import { ThemeContext, LocaleContext } from 'context';

// global
import Locale from 'global/locale';

// styles
import * as S from 'global/styles';

function Project(): React.ReactElement {
  const theme = useContext(ThemeContext);
  const locale = useContext(LocaleContext);

  const text = Locale[locale].project;

  return (
    <S.Container theme={theme}>
      <S.Heading theme={theme}>{text.heading}</S.Heading>

      <S.List theme={theme}>
        {text.items.map((item, index) => (
          <S.ListItem key={`proj-list-${index}`} theme={theme}>
            <S.Title uppercase={false} theme={theme}>
              {item.heading}
            </S.Title>

            {typeof item.description === 'string' ? (
              <S.Description>{item.description}</S.Description>
            ) : (
              item.description.map((i, index) => (
                <S.Description key={`prog-desc-list-${index}`}>
                  {i}
                </S.Description>
              ))
            )}
          </S.ListItem>
        ))}
      </S.List>

      {text.info}
    </S.Container>
  );
}

export default Project;
