// packages
import React, { useContext } from 'react';

// context
import { ThemeContext, LocaleContext } from 'context';

// global
import Locale from 'global/locale';

// styles
import * as S from 'global/styles';

function Volunteering(): React.ReactElement {
  const theme = useContext(ThemeContext);
  const locale = useContext(LocaleContext);

  const text = Locale[locale].volunteering;

  return (
    <S.Container theme={theme}>
      <S.Heading theme={theme}>{text.heading}</S.Heading>

      <S.List theme={theme}>
        {text.items.map((item, index) => (
          <S.ListItem key={`vol-list-${index}`} theme={theme}>
            <S.Time>{item.time}</S.Time>
            <S.Title uppercase={false} theme={theme}>
              {item.title}
            </S.Title>
            <S.Description theme={theme}>{item.description}</S.Description>
          </S.ListItem>
        ))}
      </S.List>
    </S.Container>
  );
}

export default Volunteering;
