// packages
import React, { useContext } from 'react';

// context
import { ThemeContext, LocaleContext } from 'context';

// global
import Locale from 'global/locale';
import { THEME, LOCALE } from 'global/enum';

// styles
import * as S from './styles';

// types
import * as T from './types';

function Header({ toggleTheme, toggleLocale }: T.Props): React.ReactElement {
  const theme = useContext(ThemeContext);
  const locale = useContext(LocaleContext);

  return (
    <S.Container theme={theme}>
      <S.Block>
        <S.Switch margin='right' theme={theme}>
          <S.ActiveBox theme={theme} left={locale === LOCALE.NEPALI} />

          <S.Case
            theme={theme}
            active={locale === LOCALE.NEPALI}
            onClick={toggleLocale}
          >
            {LOCALE.NEPALI}
          </S.Case>
          <S.Case
            theme={theme}
            active={locale === LOCALE.ENGLISH}
            onClick={toggleLocale}
          >
            {LOCALE.ENGLISH}
          </S.Case>
        </S.Switch>
      </S.Block>

      <S.Block>
        <S.Switch margin='left' theme={theme}>
          <S.ActiveBox theme={theme} left={theme === THEME.DARK} />

          <S.Case
            theme={theme}
            active={theme === THEME.DARK}
            onClick={toggleTheme}
          >
            {Locale[locale].theme[THEME.DARK]}
          </S.Case>
          <S.Case
            theme={theme}
            active={theme === THEME.LIGHT}
            onClick={toggleTheme}
          >
            {Locale[locale].theme[THEME.LIGHT]}
          </S.Case>
        </S.Switch>
      </S.Block>
    </S.Container>
  );
}

export default Header;
