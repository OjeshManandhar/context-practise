export type Props = {
  toggleTheme: () => void;
  toggleLocale: () => void;
};
