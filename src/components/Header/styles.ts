// packages
import styled from 'styled-components';

// global
import { THEME } from 'global/enum';
import { HeaderTheme } from 'global/theme';
import { Transition, HeaderHeight } from 'global/const';

export const Container = styled.header<{ theme: THEME }>`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;

  height: ${HeaderHeight};

  padding: 0.5rem 2rem;

  background-color: ${props => HeaderTheme[props.theme].bg};

  border-bottom: 0.2rem solid ${props => HeaderTheme[props.theme].border};

  ${Transition}

  & * {
    ${Transition}
  }
`;

export const Block = styled.div`
  display: inline-block;

  width: 50%;
  height: 100%;
`;

export const Switch = styled.div<{ margin: 'left' | 'right'; theme: THEME }>`
  position: relative;

  width: 20rem;
  height: 100%;

  ${props =>
    props.margin === 'left' ? 'margin-left: auto' : 'margin-right: auto'};

  text-align: center;

  overflow: hidden;

  border-radius: 1rem;
  border: 0.2rem solid ${props => HeaderTheme[props.theme].border};
`;

export const ActiveBox = styled.div<{ left: boolean; theme: THEME }>`
  position: absolute;
  top: 0;
  bottom: 0;

  left: ${props => (props.left ? '0' : '50%')};

  width: 50%;

  background-color: ${props => HeaderTheme[props.theme].activeBox};
`;

export const Case = styled.div<{
  active: boolean;
  theme: THEME;
}>`
  display: inline-block;

  position: relative;

  width: 50%;
  height: 100%;

  font-size: 1.4rem;
  line-height: 170%;

  color: ${props =>
    props.active
      ? HeaderTheme[props.theme].active
      : HeaderTheme[props.theme].color};

  border: none;
  background: none;

  cursor: pointer;

  &:hover {
    color: ${props =>
      props.active
        ? HeaderTheme[props.theme].active
        : HeaderTheme[props.theme].hover};
  }
`;
