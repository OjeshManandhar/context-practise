// packages
import styled from 'styled-components';

// global
import * as G from 'global/styles';
import { THEME } from 'global/enum';

export const About = styled(G.Description)<{ theme: THEME }>`
  margin-bottom: 0;
`;

export const Divider = styled.div`
  width: 100%;
  height: 0.1rem;

  border: none;
  background: none;

  margin-bottom: 0.25rem;
`;
