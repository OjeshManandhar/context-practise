// packages
import React, { useContext } from 'react';

// context
import { ThemeContext, LocaleContext } from 'context';

// global
import Locale from 'global/locale';

// styles
import * as S from './styles';
import * as G from 'global/styles';

function Achievements(): React.ReactElement {
  const theme = useContext(ThemeContext);
  const locale = useContext(LocaleContext);

  const text = Locale[locale].achievements;

  return (
    <G.Container theme={theme}>
      <G.Heading theme={theme}>{text.heading}</G.Heading>

      <G.List theme={theme}>
        {text.items.map((item, index) => (
          <G.ListItem key={`achiev-list-${index}`} theme={theme}>
            <G.Time>{item.time}</G.Time>
            <G.Title uppercase={false} theme={theme}>
              {item.title}
            </G.Title>

            {typeof item.about === 'string' ? (
              <S.About>{item.about}</S.About>
            ) : (
              item.about.map((i, index) => (
                <S.About key={`achiev-about-list-${index}`}>{i}</S.About>
              ))
            )}

            <S.Divider />

            {typeof item.description === 'string' ? (
              <G.Description>{item.description}</G.Description>
            ) : (
              item.description.map((i, index) => (
                <G.Description key={`achiev-desc-list-${index}`}>
                  {i}
                </G.Description>
              ))
            )}
          </G.ListItem>
        ))}
      </G.List>
    </G.Container>
  );
}

export default Achievements;
