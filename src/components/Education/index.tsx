// packages
import React, { useContext } from 'react';

// context
import { ThemeContext, LocaleContext } from 'context';

// global
import Locale from 'global/locale';

// styles
import * as S from 'global/styles';

function Education(): React.ReactElement {
  const theme = useContext(ThemeContext);
  const locale = useContext(LocaleContext);

  const text = Locale[locale].education;

  return (
    <S.Container theme={theme}>
      <S.Heading theme={theme}>{text.heading}</S.Heading>

      <S.List theme={theme}>
        {text.items.map((item, index) => (
          <S.ListItem key={`edu-list-${index}`} theme={theme}>
            <S.Time>{item.time}</S.Time>
            <S.Title uppercase theme={theme}>
              {item.title},{' '}
              <S.SutTitle theme={theme}>{item.subTitle}</S.SutTitle>
            </S.Title>
          </S.ListItem>
        ))}
      </S.List>
    </S.Container>
  );
}

export default Education;
