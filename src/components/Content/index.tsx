// packages
import React, { useContext } from 'react';

// components
import Skills from 'components/Skills';
import Project from 'components/Project';
import Education from 'components/Education';
import Experience from 'components/Experience';
import Description from 'components/Description';
import Achievements from 'components/Achievements';
import Volunteering from 'components/Volunteering';
import Participations from 'components/Participations';

// context
import { ThemeContext } from 'context';

// styles
import * as S from './styles';

function Content(): React.ReactElement {
  const theme = useContext(ThemeContext);

  return (
    <S.Container theme={theme}>
      <S.Content theme={theme}>
        <Description />

        <Experience />

        <Education />

        <Skills />

        <Project />

        <Participations />

        <Achievements />

        <Volunteering />
      </S.Content>
    </S.Container>
  );
}

export default Content;
