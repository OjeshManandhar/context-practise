// packages
import styled from 'styled-components';

// global
import { THEME } from 'global/enum';
import { ContentTheme } from 'global/theme';
import { A4Width, A4Padding, Transition, HeaderHeight } from 'global/const';

export const Container = styled.main<{ theme: THEME }>`
  width: 100%;

  background-color: ${props => ContentTheme[props.theme].bg};

  // Offset for fixed HEADER
  margin-top: ${HeaderHeight};

  ${Transition}
`;

export const Content = styled.div<{ theme: THEME }>`
  width: ${A4Width};

  color: ${props => ContentTheme[props.theme].color};

  margin: 0 auto;
  padding: ${A4Padding};

  background: none;

  ${Transition}
`;
