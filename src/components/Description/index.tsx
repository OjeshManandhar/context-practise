// packages
import React, { useContext } from 'react';

// context
import { ThemeContext, LocaleContext } from 'context';

// global
import Locale from 'global/locale';

// styles
import * as S from './styles';

function Description(): React.ReactElement {
  const theme = useContext(ThemeContext);
  const locale = useContext(LocaleContext);

  return (
    <S.Container theme={theme}>
      <S.Name>{Locale[locale].description.name}</S.Name>
      <S.Info>{Locale[locale].description.location}</S.Info>
      <S.Info>{Locale[locale].description.contact}</S.Info>
      <S.Link theme={theme} href={`mailto:${Locale[locale].description.email}`}>
        {Locale[locale].description.email}
      </S.Link>
      <S.Link
        theme={theme}
        href={`https://${Locale[locale].description.gitHub}`}
        target='_blank'
        rel='noreferrer'
      >
        {Locale[locale].description.gitHub}
      </S.Link>
      <S.Link
        theme={theme}
        href={`https://${Locale[locale].description.linkedIn}`}
        target='_blank'
        rel='noreferrer'
      >
        {Locale[locale].description.linkedIn}
      </S.Link>
      <S.About theme={theme}>{Locale[locale].description.about[0]}</S.About>
      <S.About theme={theme}>{Locale[locale].description.about[1]}</S.About>
    </S.Container>
  );
}

export default Description;
