// packages
import styled from 'styled-components';

// global
import { THEME } from 'global/enum';
import { Transition } from 'global/const';
import { ContentTheme } from 'global/theme';

export const Container = styled.section<{ theme: THEME }>`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;

  font-size: 1.2rem;
  color: ${props => ContentTheme[props.theme].text};

  ${Transition}
`;

export const Name = styled.div`
  font-size: 3.5rem;
  font-family: Georgia;
`;

export const Info = styled.div``;

export const Link = styled.a<{ theme: THEME }>`
  color: ${props => ContentTheme[props.theme].link};
  font-weight: bold;
  text-decoration: none;

  cursor: pointer;

  ${Transition}

  &:visited {
    color: ${props => ContentTheme[props.theme].link};
  }
`;

export const About = styled.p<{ theme: THEME }>`
  font-size: 1.1rem;
  text-align: center;
  line-height: 1.1rem;

  margin-bottom: 0.3rem;

  color: ${props => ContentTheme[props.theme].text};

  ${Transition}

  &:first-of-type {
    margin-top: 2rem;
  }
`;
