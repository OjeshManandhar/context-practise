// packages
import styled from 'styled-components';

// global
import * as G from 'global/styles';
import { THEME } from 'global/enum';
import { Transition } from 'global/const';
import { ContentTheme } from 'global/theme';

export const Container = G.Container;
export const Heading = G.Heading;
export const List = G.List;

export const ListItem = styled(G.ListItem)<{ theme: THEME }>`
  border: none;
`;

export const Event = styled.span<{ theme: THEME }>`
  font-size: 1.3rem;
  color: ${props => ContentTheme[props.theme].title};

  ${Transition};
`;

export const Date = styled.span<{ theme: THEME }>`
  text-transform: uppercase;
  color: ${props => ContentTheme[props.theme].text};

  ${Transition}
`;
