// packages
import React, { useContext } from 'react';

// context
import { ThemeContext, LocaleContext } from 'context';

// global
import Locale from 'global/locale';

// styles
import * as S from './styles';

function Participations(): React.ReactElement {
  const theme = useContext(ThemeContext);
  const locale = useContext(LocaleContext);

  const text = Locale[locale].participations;

  return (
    <S.Container theme={theme}>
      <S.Heading theme={theme}>{text.heading}</S.Heading>

      <S.List theme={theme}>
        {text.items.map((item, index) => (
          <S.ListItem key={`part-list-${index}`} theme={theme}>
            <S.Event theme={theme}>{item.event}</S.Event>{' '}
            <S.Date theme={theme}>{item.date}</S.Date>
          </S.ListItem>
        ))}
      </S.List>
    </S.Container>
  );
}

export default Participations;
