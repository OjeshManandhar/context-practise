// packages
import React, { useContext } from 'react';

// context
import { ThemeContext, LocaleContext } from 'context';

// global
import Locale from 'global/locale';

// styles
import * as S from 'global/styles';

function Experience(): React.ReactElement {
  const theme = useContext(ThemeContext);
  const locale = useContext(LocaleContext);

  const text = Locale[locale].experience;

  return (
    <S.Container theme={theme}>
      <S.Heading theme={theme}>{text.heading}</S.Heading>

      <S.List theme={theme}>
        {text.items.map((item, index) => (
          <S.ListItem key={`exp-list-${index}`} theme={theme}>
            <S.Time>{item.time}</S.Time>
            <S.Title uppercase theme={theme}>
              {item.title},{' '}
              <S.SutTitle theme={theme}>{item.subTitle}</S.SutTitle>
            </S.Title>
            <S.SubList>
              {item.list.map((txt, index) => (
                <S.SubListItem key={`exp-sub-list-${index}`}>
                  {txt}
                </S.SubListItem>
              ))}
            </S.SubList>
          </S.ListItem>
        ))}
      </S.List>
    </S.Container>
  );
}

export default Experience;
