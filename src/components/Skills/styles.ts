// packages
import styled from 'styled-components';

// global
import { THEME } from 'global/enum';
import { Transition } from 'global/const';
import { Heading as H, Container as C } from 'global/styles';

export const Container = styled(C)<{ theme: THEME }>`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: stretch;
  align-content: flex-start;
  flex-wrap: wrap;

  ${Transition}
`;

export const Heading = styled(H)<{ theme: THEME }>`
  width: 100%;
  min-width: 100%;
`;

export const Block = styled.div`
  width: 49%;
  min-width: 49%;

  margin: 1rem auto;
`;

export const BlockHeading = styled.h3`
  margin-left: 1.5rem;

  font-weight: bold;
`;

export const List = styled.ul`
  margin-left: 1rem;
  list-style: disc outside;
`;

export const ListItem = styled.li`
  padding-left: 0.5rem;

  &::marker {
    font-size: 1.25rem;
  }
`;
