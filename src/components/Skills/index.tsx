// packages
import React, { useContext } from 'react';

// context
import { ThemeContext, LocaleContext } from 'context';

// global
import Locale from 'global/locale';

// styles
import * as S from './styles';

function Sklls(): React.ReactElement {
  const theme = useContext(ThemeContext);
  const locale = useContext(LocaleContext);

  const text = Locale[locale].skills;

  return (
    <S.Container theme={theme}>
      <S.Heading theme={theme}>{text.heading}</S.Heading>

      <S.Block>
        <S.BlockHeading>{text.have.heading}</S.BlockHeading>

        <S.List>
          {text.have.items.map((item, index) => (
            <S.ListItem key={`skill-have-list-${index}`}>{item}</S.ListItem>
          ))}
        </S.List>
      </S.Block>

      <S.Block>
        <S.BlockHeading>{text.learning.heading}</S.BlockHeading>

        <S.List>
          {text.learning.items.map((item, index) => (
            <S.ListItem key={`skill-learning-list-${index}`}>{item}</S.ListItem>
          ))}
        </S.List>
      </S.Block>
    </S.Container>
  );
}

export default Sklls;
