// packages
import React, { useState, useCallback } from 'react';

// components
import Header from 'components/Header';
import Content from 'components/Content';

// context
import { ThemeContext, LocaleContext } from 'context';

// global
import { THEME, LOCALE } from 'global/enum';

function App(): React.ReactElement {
  const [theme, setTheme] = useState<THEME>(THEME.LIGHT);
  const [locale, setLocale] = useState<LOCALE>(LOCALE.ENGLISH);

  const toggleTheme = useCallback(() => {
    theme === THEME.DARK ? setTheme(THEME.LIGHT) : setTheme(THEME.DARK);
  }, [theme]);

  const toggleLocale = useCallback(() => {
    locale === LOCALE.NEPALI
      ? setLocale(LOCALE.ENGLISH)
      : setLocale(LOCALE.NEPALI);
  }, [locale]);

  return (
    <ThemeContext.Provider value={theme}>
      <LocaleContext.Provider value={locale}>
        <Header toggleTheme={toggleTheme} toggleLocale={toggleLocale} />
        <Content />
      </LocaleContext.Provider>
    </ThemeContext.Provider>
  );
}

export default App;
