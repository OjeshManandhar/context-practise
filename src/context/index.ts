// packages
import React from 'react';

// global
import { THEME, LOCALE } from 'global/enum';

export const ThemeContext = React.createContext<THEME>(THEME.LIGHT);

export const LocaleContext = React.createContext<LOCALE>(LOCALE.ENGLISH);
